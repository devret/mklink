# MkLink软链接

#### 介绍
##### 软链接快捷创建工具 [更新记录](https://gitee.com/devret/mklink/releases)
1. 使用的是mklink指令，生成软链接
2. 当然熟悉mklink命令的人也可以手动操作，但是可以会很麻烦
3. 写这个工具的目的是用来腾系统盘的，可以将指定目录下的文件夹软链接到其它盘上面，这样系统盘上面就空出来了，很多软件虽然可以指定安装目录。但是还有有一部分配置文件或者其它的缓存数据依然会产生在C:\Users\电脑名称\下面，特别是AppData\Local、以及AppData\Roaming中，时间长了导致系统盘越来越少。
4. 建议配合软件folderSize，这软件能看到文件夹的大小，就知道应该迁移哪些文件夹了。
5. 使用小提示：
   选择源路径后会自动填充映射路径、建议映射路径只要改下盘符就行了，保证路径的一致性，方便查看管理一点，这只是个人推荐。
#### 软件架构、原理
##### 架构：
1.  软件使用java开发，开发环境jdk17、开发工具IDEA 
2.  使用exe4j将jar转成exe文件
3.  使用Inno Setup将exe、jre打包成安装文件(安装包有30M,其中软件其实只有10M左右，剩余的全是java的运行环境jre【java这点就很恶心】)
##### 原理：
###### 比如要搬家C:\Users\text\aaa\ 目录下的文件夹到 D:\Users\text\bbb\中
1. 将aaa路径下文件夹先压缩成zip,生成的zip会放在软件的安装目录下面
2. 然后将zip解压到bbb目录中
3. 删除aaa文件夹(请先关闭相关软件，解除文件占用、涉及删除文件，不放心的话请先自己手动备份一遍)
4. 最后建立aaa与bbb的软链接


#### 安装教程
1.  开发环境jdk17
2.  软件编译后会生成jar包
3.  通过exe4j将jar转换成可执行文件
4.  最后用Inno Setup打包完成

#### 使用说明

1.  软件的作用，为目录创建软链接
2.  电脑安装的软件有些会在系统盘中生成一些配置文件，这些配置文件是不能更改位置的，就导致了系统盘占用越来越大。
3.  软件的作用就是将这些生成的目录软链接到其它盘符上，减少系统的的占用。
4. 软件截图
    ![软件图片](./src/main/resources/images/mklink.png)
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  [jar转exe](https://exe4j.apponic.com/)
2.  [打包工具](https://jrsoftware.org/)
