module com.gzzjwd.mklink {
    requires javafx.controls;
    requires javafx.fxml;
    requires cn.hutool.core;
    requires cn.hutool.http;
    requires java.sql;
    
    
    opens com.gzzjwd.mklink to javafx.fxml;
    exports com.gzzjwd.mklink;
    exports com.gzzjwd.mklink.controller;
    opens com.gzzjwd.mklink.controller to javafx.fxml;
}
