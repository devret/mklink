package com.gzzjwd.mklink.controller;

import cn.hutool.core.util.StrUtil;
import com.gzzjwd.mklink.domain.MkLink;
import com.gzzjwd.mklink.enums.Constant;
import com.gzzjwd.mklink.util.AppUtil;
import com.gzzjwd.mklink.util.FileUtil;
import com.gzzjwd.mklink.util.MkLinkFactory;
import com.gzzjwd.mklink.util.UI;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.stage.DirectoryChooser;

import java.io.File;

/**
 * 操作入口
 */
public class MkLinkController {
    @FXML
    private TextField txtSource;
    
    @FXML
    private Button btnSource;
    
    @FXML
    private TextField txtTarget;
    
    @FXML
    private Button btnTarget;
    
    @FXML
    private Button btnLink;
    
    @FXML
    private Button btnUnLink;
    
    /**
     * 单选按钮组
     */
    @FXML
    private ToggleGroup radMode;
    
    /**
     * 软链接实现类创建工厂
     */
    private MkLinkFactory mkLinkFactory = new MkLinkFactory();
    /**
     * 源路径选择事件
     */
    @FXML
    protected void onBtnSourceChooser() {
        DirectoryChooser chooser=new DirectoryChooser();
        chooser.setTitle("选择源路径");
        if(StrUtil.isNotEmpty(txtSource.getText())){
            chooser.setInitialDirectory(new File(txtSource.getText()));
        }
        File sourceFile = chooser.showDialog(btnSource.getScene().getWindow());
        if(sourceFile!=null){
            txtSource.setText(sourceFile.getAbsolutePath());
            txtTarget.setText(sourceFile.getAbsolutePath());
        }
    }
    
    /**
     * 映射路径选择事件
     */
    @FXML
    protected void onBtnTargetChooser() {
        DirectoryChooser chooser=new DirectoryChooser();
        chooser.setTitle("选择映射路径");
        if(StrUtil.isNotEmpty(txtTarget.getText()) && FileUtil.exist(txtTarget.getText())){
            chooser.setInitialDirectory(new File(txtTarget.getText()));
        }
        File sourceFile = chooser.showDialog(btnTarget.getScene().getWindow());
        if(sourceFile!=null){
            txtTarget.setText(sourceFile.getAbsolutePath());
        }
    }
    
    /**
     * 设置软连接
     */
    @FXML
    protected void onBtnLink() {
        UI.setText("设置中...",btnLink);
        disable(true);
        //获取单选框的值
        String mode  = radMode.getSelectedToggle().getUserData().toString();
        //根据mode值通过工厂获取具体调用哪个实现类
        mkLinkFactory.getFactory(mode).mkLink(new MkLink(txtSource.getText(),txtTarget.getText(),mode));
        UI.setText("设置软链接",btnLink);
        disable(false);
    }
    
    /**
     * 取消软连接
     */
    @FXML
    protected void onBtnUnLink() {
        UI.setText("取消中...",btnUnLink);
        disable(true);
        //获取单选框的值
        String mode  = radMode.getSelectedToggle().getUserData().toString();
        //根据mode值通过工厂获取具体调用哪个实现类
        mkLinkFactory.getFactory(mode).unMkLink(new MkLink(txtSource.getText(),txtTarget.getText(),mode));
        UI.setText("取消软连接",btnUnLink);
        disable(false);
    }
    
    /**
     * 打开项目地址
     */
    @FXML
    protected void onBtnOpenUrl() {
        AppUtil.openBrowser(Constant.PROJECT_GIT.getCode());
    }
    
    /**
     * 禁用、启用控件
     * @param disable
     */
    private void disable(Boolean disable){
        UI.setDisable(disable,txtTarget,btnSource,btnTarget,btnLink,btnUnLink);
    }
}
