package com.gzzjwd.mklink;

import com.gzzjwd.mklink.util.AppUtil;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;

public class MkLinkApplication extends Application {
    
    @Override
    public void start(Stage stage) throws IOException {
        Image image = new Image("icon.png");
        stage.getIcons().add(image);
        FXMLLoader fxmlLoader = new FXMLLoader(MkLinkApplication.class.getResource("main-view.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        stage.setTitle("MkLink "+ AppUtil.getAppVersion() +" - 52Pojie By w678");
        stage.setScene(scene);
        stage.show();
        
        //检查更新
        AppUtil.checkUpdate();
    }
    
    public static void main(String[] args) {
        launch();
    }
}
