package com.gzzjwd.mklink.domain;

/**
 * @Author: wyx
 * @Date: 2023/8/20 19:07
 */
public class MkLink {
    /**
     * 源路径
     */
    private String sourcePath;
    /**
     * 映射路径
     */
    private String targetPath;
    /**
     * 单按钮值 J D H
     */
    private String mode;
    
    public MkLink(){}
    
    public MkLink(String sourcePath, String targetPath, String mode) {
        this.sourcePath = sourcePath;
        this.targetPath = targetPath;
        this.mode = mode;
    }
    
    public String getSourcePath() {
        return sourcePath;
    }
    
    public void setSourcePath(String sourcePath) {
        this.sourcePath = sourcePath;
    }
    
    public String getTargetPath() {
        return targetPath;
    }
    
    public void setTargetPath(String targetPath) {
        this.targetPath = targetPath;
    }
    
    public String getMode() {
        return mode;
    }
    
    public void setMode(String mode) {
        this.mode = mode;
    }
}
