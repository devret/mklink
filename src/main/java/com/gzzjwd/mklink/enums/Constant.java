package com.gzzjwd.mklink.enums;

/**
 * 枚举常量
 * @Author: wyx
 * @Date: 2023/8/26 10:34
 */
public enum Constant {
    PROJECT_GIT("https://gitee.com/devret/mklink.git"),
    PROJECT_RELEASES("https://gitee.com/devret/mklink/releases/"),
    PROJECT_TAG("https://gitee.com/devret/mklink/releases/tag/"),
    END("");
    
    private String code;
    
    private Constant(String code){
        this.code=code;
    }
    
    /**
     * 定义方法,返回描述,跟常规类的定义没区别
     * @return
     */
    public String getCode(){
        return code;
    }
}
