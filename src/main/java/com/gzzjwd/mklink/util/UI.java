package com.gzzjwd.mklink.util;

import javafx.application.Platform;
import javafx.scene.control.Control;
import javafx.scene.control.Labeled;

/**
 * @Author: wyx
 * @Date: 2023/8/24 9:31
 */
public class UI {
    
    /**
     * 更新UI控件标题
     * @param control 控件
     * @param text 文字
     * @param <T> Labeled的子类
     */
    public static <T extends Labeled> void setText(String text,T control){
        Platform.runLater(new Runnable(){
            @Override
            public void run() {
                control.setText(text);
            }
        });
    }
    
    
    /**
     * 禁用控件
     * @param disable true-禁用、false-不禁用
     * @param controls 控件列表
     * @param <T> Control的子类
     */
    public static <T extends Control> void setDisable(Boolean disable, T... controls){
        Platform.runLater(new Runnable(){
            @Override
            public void run() {
                for (T c : controls) {
                    c.setDisable(disable);
                }
            }
        });
    }
}
