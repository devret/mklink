package com.gzzjwd.mklink.util;

import cn.hutool.core.thread.ThreadUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.RuntimeUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import com.gzzjwd.mklink.enums.Constant;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.io.IOException;
import java.util.Optional;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Author: wyx
 * @Date: 2023/8/24 15:50
 */
public class AppUtil {
    /**
     * 获取pom版本号
     * @return
     */
    private static String getPomVersion() {
        String appVersion = null;
        if (null == appVersion) {
            Properties properties = new Properties();
            try {
                properties.load(AppUtil.class.getClassLoader().getResourceAsStream("app.properties"));
                if (!properties.isEmpty()) {
                    appVersion = properties.getProperty("app.version");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return appVersion;
    }
    
    /**
     * 获取值字符串版本号
     * @return
     */
    public static String getAppVersion() {
        return "v"+getPomVersion();
    }
    
    /**
     * 获取下一个版本
     * @return
     */
    private static String getNextAppVersion() {
        String appVersion = getPomVersion();
        if(StrUtil.isNotEmpty(appVersion)){
            double appNum = Double.valueOf(appVersion);
            appVersion =String.valueOf(NumberUtil.add(appNum,0.1D));
        }
        return "v"+appVersion;
    }
    
    /**
     * 检测版本更新
     */
    public static void checkUpdate(){
        ThreadUtil.execAsync(new Runnable() {
            @Override
            public void run() {
                String html = HttpUtil.get(Constant.PROJECT_TAG.getCode()+getNextAppVersion());
                if(StrUtil.isNotEmpty(html)){
                    //取title中的标签
                    String title = get("<title>(.*?)</title>",html,0);
                    //取版本号
                    if(StrUtil.isNotEmpty(title)){
                        String version = get("v\\d\\.\\d",title,0);
                        if(StrUtil.isNotEmpty(version)){
                            //判断版本号是否一致
                            String locVersion = getAppVersion();
                            if(!version.equals(locVersion)){
                                Platform.runLater(new Runnable(){
                                    @Override
                                    public void run() {
                                        //弹窗
                                        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                                        alert.setTitle("提示");
                                        alert.setHeaderText(null);
                                        alert.setContentText("有新版本更新！前往查看?");
                                        
                                        Optional<ButtonType> result = alert.showAndWait();
                                        if (result.get() == ButtonType.OK){
                                            //打开项目地址
                                            openBrowser(Constant.PROJECT_RELEASES.getCode());
                                        }
                                    }
                                });
                                
                            }
                        }
                    }
                }
               
            }
        });
    }
    
    /**
     * 获得匹配的字符串
     *
     * @param pattern 编译后的正则模式
     * @param content 被匹配的内容
     * @param groupIndex 匹配正则的分组序号
     * @return 匹配后得到的字符串，未匹配返回null
     */
    public static String get(Pattern pattern, String content, int groupIndex) {
        Matcher matcher = pattern.matcher(content);
        if (matcher.find()) {
            return matcher.group(groupIndex);
        }
        return null;
    }
    
    /**
     * 获得匹配的字符串
     *
     * @param regex 匹配的正则
     * @param content 被匹配的内容
     * @param groupIndex 匹配正则的分组序号
     * @return 匹配后得到的字符串，未匹配返回null
     */
    public static String get(String regex, String content, int groupIndex) {
        Pattern pattern = Pattern.compile(regex, Pattern.DOTALL);
        return get(pattern, content, groupIndex);
    }
    
    /**
     * 打开浏览器
     * @param url
     */
    public static void openBrowser(String url){
        StringBuffer sb = new StringBuffer();
        sb.append("cmd /c start ");
        sb.append(url);
        RuntimeUtil.exec(sb.toString());
    }
}
