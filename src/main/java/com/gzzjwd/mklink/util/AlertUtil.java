package com.gzzjwd.mklink.util;

import javafx.scene.control.Alert;

/**
 * 提示框工具类
 * @Author: wyx
 * @Date: 2023/8/20 11:00
 */
public class AlertUtil {
    public static void information(String msg){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("提示");
        alert.setHeaderText(null);
        alert.setContentText(msg);
        alert.showAndWait();
    }
}
