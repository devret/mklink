package com.gzzjwd.mklink.util;

import com.gzzjwd.mklink.service.IMkLinkService;
import com.gzzjwd.mklink.service.impl.DMkLinkServiceImpl;
import com.gzzjwd.mklink.service.impl.HMkLinkServiceImpl;
import com.gzzjwd.mklink.service.impl.JMkLinkServiceImpl;

/**
 * 创建软件链接实现类工程
 * @Author: wyx
 * @Date: 2023/8/20 19:33
 */
public class MkLinkFactory {
    /**
     * 根据单选框mode的值实例化具体的实现类
     * @param mode J D H
     * @return
     */
    public IMkLinkService getFactory(String mode){
        IMkLinkService mkLinkService = null;
        switch (mode){
            case "J":
                mkLinkService = new JMkLinkServiceImpl();
                break;
            case "D":
                mkLinkService = new DMkLinkServiceImpl();
                break;
            case "H":
                mkLinkService = new HMkLinkServiceImpl();
                break;
            default:
                break;
        }
        return  mkLinkService;
    }
}
