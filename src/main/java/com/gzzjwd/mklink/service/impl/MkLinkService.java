package com.gzzjwd.mklink.service.impl;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.gzzjwd.mklink.domain.MkLink;
import com.gzzjwd.mklink.service.IMkLinkService;
import com.gzzjwd.mklink.util.AlertUtil;
import com.gzzjwd.mklink.util.FileUtil;

import java.io.File;
import java.io.IOException;
import java.util.Date;

/**
 * 软链接
 * @Author: wyx
 * @Date: 2023/8/20 19:05
 */
public abstract class MkLinkService implements IMkLinkService {
    
    /**
     * 创建链接
     * @param link
     * @return
     */
    @Override
    public final boolean mkLink(MkLink link){
        if(StrUtil.isEmpty(link.getSourcePath())){
            AlertUtil.information("源路径不能为空！");
            return false;
        }
        if(StrUtil.isEmpty(link.getTargetPath())){
            AlertUtil.information("映射路径不能为空！");
            return false;
        }
        if(link.getTargetPath().equals(link.getSourcePath())){
            AlertUtil.information("源路径与映射路径相同，请检查！");
            return false;
        }
        
        //判断是否已经是软链接
        try {
            if(FileUtil.isLink(new File(link.getSourcePath()))){
                AlertUtil.information("源路径已经是软链接！");
                return false;
            }
        }catch (IOException e){
            AlertUtil.information("源路径已经是软链接！");
            return false;
        }
        //判断是否正确的路径盘符
        if(!FileUtil.validPath(link.getTargetPath())){
            AlertUtil.information("映射路径无效，请检查！");
            return false;
        }
        return doMkLink(link);
    }
    
    /**
     * 取消链接
     * @param link
     * @return
     */
    @Override
    public final boolean unMkLink(MkLink link){
        if(StrUtil.isEmpty(link.getSourcePath())){
            AlertUtil.information("源路径不能为空！");
            return false;
        }
        
        try {
            if(!FileUtil.isLink(new File(link.getSourcePath()))){
                AlertUtil.information("源路径非软链接！");
                return false;
            }
        }catch (IOException e){
            AlertUtil.information("源路径非软链接！");
            return false;
        }
        
        return doUnMkLink(link);
    }
    
    /**
     * 实际业务逻辑创建链接：由子类实现
     * @param link
     * @return
     */
    protected abstract boolean doMkLink(MkLink link);
    
    /**
     * 实际业务逻辑取消链接：由子类实现
     * @param link
     * @return
     */
    protected abstract boolean doUnMkLink(MkLink link);
    /**
     * 取目录的文件夹名称
     * @param path
     * @return
     */
    private String getLastPathName(String path){
        if(StrUtil.isNotEmpty(path)){
            int lastNameIndex = StrUtil.lastIndexOfIgnoreCase(path,"\\");
            String name = StrUtil.sub(path,lastNameIndex+1,StrUtil.length(path));
            return name;
        }
        return "";
    }
    
    /**
     * 压缩包的备份路径：当前软件安装路径下的/backup/2023/01/01/test_12_00_00.zip
     * @param path
     * @return
     */
    public String getBackupName(String path){
        return getBackupNamePath(path)+".zip";
    }
    
    /**
     * 获取备份路径
     * @return
     */
    public String getBackupPath(){
        return "backup\\" + DateUtil.thisYear()+"\\"+(DateUtil.thisMonth()+1)+"\\"+DateUtil.thisDayOfMonth()+"\\";
    }
    
    /**
     * 获取备份路径+源文件名称路径
     * @return
     */
    public String getBackupNamePath(String path){
        return getBackupPath()+getLastPathName(path)+"_"+ DateUtil.format(new Date(), DatePattern.NORM_TIME_PATTERN).replaceAll(":","_");
    }
    
    /**
     * 根据mode获取cmd命令
     * @return
     */
    public String getCmdByMode(String sourcePath,String targetPath,String mode){
        StringBuffer sb = new StringBuffer();
        sb.append("cmd /c").append(" ");
        sb.append("mklink /").append(mode).append(" ");
        sb.append("\"").append(sourcePath).append("\"").append(" ");
        sb.append("\"").append(targetPath).append("\"");
        return sb.toString();
    }
}
