package com.gzzjwd.mklink.service.impl;

import com.gzzjwd.mklink.domain.MkLink;

/**
 * /H 为文件创建硬链接，属于硬链接；
 * @Author: wyx
 * @Date: 2023/8/20 12:35
 */
public class HMkLinkServiceImpl extends MkLinkService {
    
    @Override
    public boolean doMkLink(MkLink link){
       //TODO 待实现
        return true;
    }
    
    @Override
    protected boolean doUnMkLink(MkLink link) {
        //TODO 待实现
        return false;
    }
}
