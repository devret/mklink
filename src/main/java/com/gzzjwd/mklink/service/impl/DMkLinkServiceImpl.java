package com.gzzjwd.mklink.service.impl;

import com.gzzjwd.mklink.domain.MkLink;

/**
 * /D 为目录创建符号链接，属于软链接，允许相对路径或绝对路径的链接；
 * @Author: wyx
 * @Date: 2023/8/20 12:35
 */
public class DMkLinkServiceImpl extends MkLinkService {
    
    @Override
    public boolean doMkLink(MkLink link){
        //TODO 待实现
        return true;
    }
    
    @Override
    protected boolean doUnMkLink(MkLink link) {
        //TODO 待实现
        return false;
    }
}
