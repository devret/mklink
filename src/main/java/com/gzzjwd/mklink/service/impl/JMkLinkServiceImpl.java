package com.gzzjwd.mklink.service.impl;

import cn.hutool.core.util.RuntimeUtil;
import cn.hutool.core.util.ZipUtil;
import com.gzzjwd.mklink.domain.MkLink;
import com.gzzjwd.mklink.util.AlertUtil;
import com.gzzjwd.mklink.util.FileUtil;

import java.io.File;

/**
 * /J 为目录创建联接点，属于软链接，只能是绝对路径的链接。
 * @Author: wyx
 * @Date: 2023/8/20 12:35
 */
public class JMkLinkServiceImpl extends MkLinkService {
    
    @Override
    public boolean doMkLink(MkLink link){
        //判断文件夹是否存在、不存在则创建
        if(!FileUtil.exist(link.getTargetPath())){
            File targetFilePath = FileUtil.mkdir(link.getTargetPath());
            if(!FileUtil.exist(targetFilePath)){
                AlertUtil.information("创建映射文件夹失败！");
                return false;
            }
        }
        //获取备份文件名称
        String zipName = getBackupName(link.getSourcePath());
        try {
            //压缩源文件下的资源文件，保存
            ZipUtil.zip(link.getSourcePath(), zipName);
        }catch (Exception e){
            AlertUtil.information("压缩"+link.getSourcePath()+"失败：\n"+e.getMessage());
            FileUtil.del(zipName);
            return false;
        }
        
        try {
            //删除源文件夹
            if(!FileUtil.del(link.getSourcePath())){
                AlertUtil.information("删除"+link.getSourcePath()+"失败！");
                FileUtil.del(zipName);
                return false;
            }
        }catch (Exception e){
            AlertUtil.information("1、删除"+link.getSourcePath()+"失败\n2、文件可能被占用，请关闭相关程序后再试！\n3、本次操作不生效、不影响任何文件！\n"+e.getMessage());
            try {
                //解压回原来的路径
                ZipUtil.unzip(zipName, link.getSourcePath());
            }catch (Exception unzipEx){
                //如果解压回去时候失败，(一般是源路径下的部分文件被占用导致的)则把压缩包单独解压一份，单个文件单独移动过去
                String unzipPath = getBackupNamePath(link.getSourcePath());
                ZipUtil.unzip(zipName,unzipPath);
                //移动
                FileUtil.moveFile(link.getSourcePath(),unzipPath, cn.hutool.core.io.FileUtil.loopFiles(unzipPath));
            }
            return false;
        }
        
        //解压文件到映射文件夹
        ZipUtil.unzip(zipName, link.getTargetPath());
        //组装cmd命令
        String cmd = getCmdByMode(link.getSourcePath(),link.getTargetPath(),link.getMode());
        //获取执行的返回结果
        String cmdRet = RuntimeUtil.execForStr(cmd);
        //判断是否链接成功
        boolean ret = cmdRet.indexOf("<<===>>")>-1;
        if (ret) {
            AlertUtil.information("链接成功：\n" + cmdRet);
        } else {
            AlertUtil.information("链接失败：\n" + cmdRet);
        }
        return ret;
    }
    
    /**
     * 解除软链接
     * @param link
     * @return
     */
    @Override
    protected boolean doUnMkLink(MkLink link) {
        String zipName = getBackupName(link.getSourcePath());
        try {
            //压缩源路径下的资源文件，保存
            ZipUtil.zip(link.getSourcePath(), zipName);
        }catch (Exception e){
            AlertUtil.information("压缩"+link.getSourcePath()+"失败：\n"+e.getMessage());
            FileUtil.del(zipName);
            return false;
        }
        
        //删除源路径
        if(!FileUtil.del(link.getSourcePath())){
            AlertUtil.information("删除"+link.getSourcePath()+"失败！");
            FileUtil.del(zipName);
            return false;
        }
        
        //创建源路径
        FileUtil.mkdir(link.getSourcePath());
        //解压文件到源路径
        ZipUtil.unzip(zipName, link.getSourcePath());
        AlertUtil.information("取消软链接成功！");
        return true;
    }
}
