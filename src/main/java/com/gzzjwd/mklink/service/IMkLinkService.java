package com.gzzjwd.mklink.service;

import com.gzzjwd.mklink.domain.MkLink;

/**
 * @Author: wyx
 * @Date: 2023/8/20 18:53
 */
public interface IMkLinkService {
   /**
    * 创建链接
    * @param link
    * @return
    */
   boolean mkLink(MkLink link);
   
   /**
    * 取消链接
    * @param link
    * @return
    */
   boolean unMkLink(MkLink link);
   
}
